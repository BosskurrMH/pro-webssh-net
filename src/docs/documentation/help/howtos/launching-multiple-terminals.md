---
title: Multiple connections
---
# How to launch multiple SSH or SFTP connections?

## macOS
In order to use multiple features at same time (SSH, SFTP, mashREPL, ping, ...), you can either launch a new window (<code>Cmd + N</code>) or a new tab :

1. macOS Menu Bar > View
2. Show All Tabs
3. Press the "+" button

## iPad
You can either launch a new window or split one :

1. Press the three dots "..." on top of your iPad status bar
2. Press (+) to add a new window or choose to split the screen

## iPhone
It's easy as :

1. Launch a SSH or SFTP connection
2. Press the "multiple windows" top left icon on the navigation bar
3. Choose "New tab"
4. Launch another SSH or SFTP connection
5. Switch between connections by using the "multiple windows" top left icon on the navigation bar
